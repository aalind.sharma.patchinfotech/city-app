import React, { Component } from 'react';
import { logout } from '../../services/authService';
import * as AiIcons from 'react-icons/ai';

import './navbar.css';

class Navbar extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	handleLogout = () => {
		logout();
		window.location = '/login';
	};

	render() {
		return (
			<div id='navbar'>
				<div
					style={{
						display: 'flex',
						alignItems: 'center',
					}}
				>
					{this.props.history.location.pathname ===
					'/catalogue/categories/new' ? (
						<>
							<span>
								<AiIcons.AiOutlineArrowLeft
									style={{ minHeight: '44px', minWidth: '44px' }}
									onClick={() =>
										this.props.history.push('/catalogue/categories')
									}
								/>
								<span style={{ marginLeft: '20px', fontSize: '20px' }}>
									Add new category
								</span>
							</span>
						</>
					) : (
						<span style={{ marginLeft: '20px', fontSize: '20px' }}>
							Catalogue
						</span>
					)}
				</div>

				<button className='btn btn-danger' onClick={this.handleLogout}>
					Logout
				</button>
			</div>
		);
	}
}

export default Navbar;
