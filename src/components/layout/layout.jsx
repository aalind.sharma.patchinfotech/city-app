import React, { Component } from 'react';
import { Switch, Redirect } from 'react-router-dom';

import AddCategory from '../addCategory/addCategory';
// import AllCategories from '../allCategories/allCategories';
// import Products from '../products/products';
import ProtectedRoute from '../common/protectedRoute';
import Home from '../home/home';
import Sidebar from '../sidebar/sidebar';
import Navbar from '../navbar/navbar';
import Catalogue from '../catalogue/catalogue';

class Layout extends Component {
	render() {
		return (
			<>
				<ProtectedRoute component={Navbar} />
				<ProtectedRoute component={Sidebar} />
				<div
					id='content'
					style={{
						marginLeft: '224px',
						padding: '30px 30px 0px 30px',
						backgroundColor: '#F7F7F7',
					}}
				>
					<Switch>
						<ProtectedRoute
							path='/catalogue/categories/:id/edit'
							component={AddCategory}
						/>
						<ProtectedRoute
							path='/catalogue/categories/new'
							component={AddCategory}
						/>
						{/* <ProtectedRoute
							path='/catalogue/categories'
							component={AllCategories}
						/> */}
						{/* <ProtectedRoute path='/catalogue/products' component={Products} /> */}
						<ProtectedRoute path='/catalogue' component={Catalogue} />
						<ProtectedRoute path='/' component={Home} exact />
						<Redirect to='/not-found' />
					</Switch>
				</div>
			</>
		);
	}
}

export default Layout;
