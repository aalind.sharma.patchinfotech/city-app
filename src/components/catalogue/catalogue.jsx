import React, { Component } from 'react';
import { Switch } from 'react-router-dom';
import AllCategories from '../allCategories/allCategories';
import ProtectedRoute from '../common/protectedRoute';
import Products from '../products/products';

class Catalogue extends Component {
	render() {
		return (
			<Switch>
				<ProtectedRoute path='/catalogue/products' component={Products} />
				<ProtectedRoute
					path='/catalogue/categories'
					component={AllCategories}
				/>
			</Switch>
		);
	}
}

export default Catalogue;
