import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './menu.css';

class Menu extends Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false,
		};
	}

	componentDidMount = () => {
		let currentURL = this.props.location.pathname;
		let { item } = this.props;

		if (!item.subNav) {
			if (currentURL === item.path) {
				this.setState({ open: true });
			}
		} else if (item.subNav) {
			item.subNav.forEach((e) => {
				if (e.path === currentURL) {
					this.setState({ open: true });
					return;
				}
			});
		} else {
			!this.state.open && this.setState({ open: false });
		}
	};

	componentDidUpdate = (prevProps, prevState) => {
		let currentURL = this.props.location.pathname;
		let { item } = this.props;

		if (prevProps.location.pathname === this.props.location.pathname) return;

		if (!item.subNav) {
			currentURL === item.path
				? this.setState({ open: true })
				: this.setState({ open: false });
		} else if (item.subNav) {
			for (const val of item.subNav) {
				if (val.path === currentURL) {
					this.setState({ open: true });
					return;
				}
			}
			this.setState({ open: false });
		} else {
			!this.state.open && this.setState({ open: false });
		}
	};

	render() {
		let { item } = this.props;
		let { title, path, icon, subNav } = item;
		let { open } = this.state;

		return (
			<>
				<li key={title} className='nav-text'>
					<Link to={path} className={open ? 'active' : ''}>
						{icon}
						<span style={{ marginLeft: '16px' }}>{title}</span>
					</Link>
				</li>
				{subNav &&
					open &&
					subNav.map((item) => {
						return (
							<div key={item.title} className='dropdown-link'>
								<Link
									to={item.path}
									className={
										this.props.location.pathname === item.path ? 'active' : ''
									}
								>
									{item.title}
								</Link>
							</div>
						);
					})}
			</>
		);
	}
}

export default Menu;
