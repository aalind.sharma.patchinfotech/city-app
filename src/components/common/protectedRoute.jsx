import React from 'react';
import { Route, Redirect } from 'react-router';

import { getUser } from '../../services/authService';

function ProtectedRoute({ component: Component, render, ...rest }) {
	return (
		<Route
			{...rest}
			render={(props) => {
				if (!getUser()) return <Redirect to='/login' />;
				return Component ? <Component {...props} /> : render(props);
			}}
		/>
	);
}

export default ProtectedRoute;
