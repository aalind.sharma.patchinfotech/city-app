import React from 'react';
import { Link } from 'react-router-dom';
import './buttonLink.css';

function ButtonLink(props) {
	let { path, styles } = props;

	return (
		<Link
			to={path}
			className='btn btn-primary'
			id='button-link'
			style={styles ? { ...styles } : null}
		>
			Add new category
		</Link>
	);
}

export default ButtonLink;
