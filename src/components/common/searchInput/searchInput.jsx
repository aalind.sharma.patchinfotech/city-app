import React from 'react';
import './searchInput.css';

const SearchInput = (props) => {
	let { searchText, placeholder, onInputChange } = props;

	return (
		<div id='catalogue-search'>
			<button>
				<i className='fa fa-search'></i>
			</button>
			<input
				type='search'
				value={searchText}
				placeholder={placeholder}
				onChange={onInputChange}
			/>
		</div>
	);
};

export default SearchInput;
