import React, { Component } from 'react';
import './addCategory.css';

import { saveCategory, getCategory } from '../../services/categoryService';
import UploadSVG from '../../assets/images/upload.svg';
import CloseIcon from '../../assets/images/close.svg';

class AddCategory extends Component {
	constructor(props) {
		super(props);
		this.state = {
			uploadedImg: null,
			name: '',
			inStock: true,
			products: 0,
			hidden: false,
		};
	}

	componentDidMount = () => {
		let categoryId = this.props.match.params.id;
		debugger;
		if (categoryId) {
			let categoryData = getCategory(categoryId);
			this.setState({
				...categoryData,
				uploadedImg: categoryData.image,
			});
		}
	};

	onImgUpload = (event) => {
		this.setState({ uploadedImg: URL.createObjectURL(event.target.files[0]) });
	};

	onNameChange = (event) => {
		let name = event.target.value;
		this.setState({ name });
	};

	onSubmit = (event) => {
		event.preventDefault();
		saveCategory(this.state);
		this.props.history.push('/catalogue/categories');
	};

	render() {
		return (
			<div id='add-category-parent'>
				<form id='add-category-form'>
					<div className='card category-card' style={{ marginBottom: '20px' }}>
						<h4 style={{ fontSize: '16px', marginBottom: '24px' }}>
							Category Image
						</h4>
						<div id='category-img-wrap'>
							<span
								id='file-img-upload'
								style={{
									position: 'relative',
									width: '100%',
									height: '100%',
								}}
							>
								{this.state.uploadedImg ? (
									<>
										<img
											src={this.state.uploadedImg}
											alt=''
											style={{
												width: '100%',
												height: '100%',
												objectFit: 'cover',
											}}
										/>
										<img
											src={CloseIcon}
											alt=''
											style={{ position: 'absolute', top: '5px', right: '5px' }}
											onClick={() => this.setState({ uploadedImg: null })}
										/>
									</>
								) : (
									<>
										<input
											type='file'
											accept='image/*'
											onChange={this.onImgUpload}
											style={{ display: 'none' }}
											ref={(imgInput) => (this.imgInput = imgInput)}
										/>
										<img
											src={UploadSVG}
											alt='upload-icon'
											onClick={() => this.imgInput.click()}
										/>
									</>
								)}
							</span>
						</div>
					</div>

					<div className='card category-card' id='category-information-section'>
						<h4 style={{ fontSize: '16px', marginBottom: '24px' }}>
							Category Information
						</h4>

						<div style={{ maxWidth: '50%' }}>
							<label id='category-label'>
								Category Name <span>*</span>
							</label>
							<input
								type='text'
								placeholder='Enter category name'
								name='name'
								maxLength='30'
								autoComplete='off'
								value={this.state.name}
								onChange={this.onNameChange}
								className='form-control'
								style={{
									fontSize: '18px',
									padding: '11px 16px',
								}}
							/>
						</div>
					</div>
					<div style={{ display: 'flex', justifyContent: 'end' }}>
						<button
							className='btn btn-primary btn-md'
							type='submit'
							onClick={this.onSubmit}
						>
							{this.props.match.params.id ? 'Update Category' : 'Add Category'}
						</button>
					</div>
				</form>
			</div>
		);
	}
}

export default AddCategory;
