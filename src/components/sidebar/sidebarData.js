import React from 'react';
import * as AiIcons from 'react-icons/ai';
import * as BiIcons from 'react-icons/bi';
import * as RiIcons from 'react-icons/ri';

export const SidebarData = [
	{
		title: 'Home',
		path: '/',
		icon: <AiIcons.AiFillHome />,
	},
	{
		title: 'Orders',
		path: '/orders',
		icon: <BiIcons.BiNotepad />,
	},
	{
		title: 'Products',
		path: '/catalogue/products',
		icon: <BiIcons.BiBorderAll />,
		iconOpened: <RiIcons.RiArrowUpSFill />,
		iconClosed: <RiIcons.RiArrowDownSFill />,
		subNav: [
			{
				title: 'All products',
				path: '/catalogue/products',
			},
			{
				title: 'Categories',
				path: '/catalogue/categories',
			},
		],
	},
];
