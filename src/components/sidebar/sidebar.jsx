import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { SidebarData } from './sidebarData';
import { IconContext } from 'react-icons';
import './sidebar.css';
import Menu from '../menu/menu';

class Sidebar extends Component {
	render() {
		return (
			<div>
				<IconContext.Provider value={{ color: '#fff' }}>
					<nav className='nav-menu'>
						<div className='nav-heading'>
							<Link to='/'>
								<img
									src='https://web.mydukaan.io/images/dukaan-premium.svg'
									alt='logo'
								/>
							</Link>
						</div>
						<ul className='nav-menu-items'>
							{SidebarData.map((item) => {
								return <Menu key={item.title} item={item} {...this.props} />;
							})}
						</ul>
					</nav>
				</IconContext.Provider>
			</div>
		);
	}
}

export default Sidebar;
