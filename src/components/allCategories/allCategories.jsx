import React, { Component } from 'react';
import './allCategories.css';
import { getCategories } from '../../services/categoryService';
// import listViewIcon from '../../assets/images/list-view.svg';
// import gridViewIcon from '../../assets/images/grid-view.svg';
import eyeIcon from '../../assets/images/eye.svg';
import shareIcon from '../../assets/images/share.svg';
import SearchInput from '../common/searchInput/searchInput';
import ButtonLink from '../common/buttonLink/buttonLink';

class AllCategories extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: '',
			allCategories: getCategories(),
			filteredCategories: getCategories(),
		};
	}

	onSwitchToggle = (category) => {
		const allCategories = [...this.state.allCategories];
		let idx = allCategories.indexOf(category);
		allCategories[idx] = { ...allCategories[idx] };
		allCategories[idx].inStock = !allCategories[idx].inStock;

		const filteredCategories = [...this.state.filteredCategories];
		idx = filteredCategories.indexOf(category);
		filteredCategories[idx] = { ...filteredCategories[idx] };
		filteredCategories[idx].inStock = !filteredCategories[idx].inStock;

		this.setState({
			allCategories,
			filteredCategories,
		});
	};

	onSearchChange = (event) => {
		let search = event.target.value;
		let allCategories = this.state.allCategories;
		let filteredCategories = allCategories.filter((c) =>
			c.name.includes(search)
		);
		this.setState({ search, filteredCategories });
	};

	handleEdit = (categoryData) => {
		return this.props.history.push(
			`/catalogue/categories/${categoryData._id}/edit`
		);
	};

	render() {
		return (
			<div id='catalogue-page'>
				<div id='catalogue-header'>
					<SearchInput
						searchText={this.state.search}
						placeholder={'Search categories..'}
						onInputChange={this.onSearchChange}
					/>
					<div id='catalogue-actions'>
						{/* <div id='list-view'>
							<img src={listViewIcon} alt='list-view-icon' />
						</div>
						<div id='grid-view'>
							<img src={gridViewIcon} alt='grid-view-icon' />
						</div> */}
						<div id='catalogue-header-btn'>
							<ButtonLink path={'/catalogue/categories/new'} />
						</div>
					</div>
				</div>

				<div id='catalogue-body'>
					<div className='card' id='catalogue-table'>
						<div id='catalogue-table-header'>
							<div className='catalogue-table-header-content'>Category</div>
							<div className='catalogue-table-header-content'>Products</div>
							<div className='catalogue-table-header-content'>Status</div>
							<div className='catalogue-table-header-content'>Action</div>
						</div>

						<div id='catalogue-table-body'>
							{this.state.filteredCategories.map((category) => (
								<div className='catalogue-table-row' key={category._id}>
									<div>
										<img
											src={category.image}
											alt={category.title}
											style={{ width: '48px', height: '48px' }}
										/>
										{/* {category.image()} */}
										<span style={{ marginLeft: '20px' }}>{category.name}</span>
									</div>
									<div>{category.products}</div>
									<div>
										<div className='form-switch'>
											<input
												className='form-check-input'
												type='checkbox'
												role='switch'
												id='flexSwitchCheckDefault'
												checked={category.inStock}
												onChange={() => this.onSwitchToggle(category)}
											/>
										</div>
										{category.inStock}
									</div>
									<div>
										<span>
											<img src={eyeIcon} alt='eye-icon' />
										</span>
										<span style={{ marginLeft: '10px' }}>
											<img src={shareIcon} alt='share-icon' />
										</span>
										<span
											style={{ marginLeft: '10px', cursor: 'pointer' }}
											onClick={() => this.handleEdit(category)}
										>
											<i className='fa fa-ellipsis-v'></i>
										</span>
									</div>
								</div>
							))}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default AllCategories;
