const categories = [
	{
		_id: Math.random().toString().split('.')[1],
		name: 'grocery',
		image:
			'https://cdn.mydukaan.io/app/image/300x300/?url=https://api.mydukaan.io/static/images/category-def.jpg',
		products: 10,
		inStock: true,
		hidden: false,
	},
	{
		_id: Math.random().toString().split('.')[1],
		name: 'clothing',
		image:
			'https://cdn.mydukaan.io/app/image/300x300/?url=https://api.mydukaan.io/static/images/category-def.jpg',
		products: 4,
		inStock: false,
		hidden: false,
	},
];

export function getCategories() {
	return categories.filter((c) => c);
}

export function getCategory(id) {
	debugger;
	return categories.find((c) => c._id === id);
}

export function saveCategory(category) {
	let categoryInDB = categories.find((c) => c._id === category._id) || {};
	categoryInDB.name = category.name;
	if (!category.uploadedImg) {
		category.image =
			'https://cdn.mydukaan.io/app/image/300x300/?url=https://api.mydukaan.io/static/images/category-def.jpg';
	}
	categoryInDB.image = category.image || category.uploadedImg;
	categoryInDB.products = category.products;
	categoryInDB.inStock = category.inStock;
	categoryInDB.hidden = category.hidden;

	if (!categoryInDB._id) {
		categoryInDB._id = Math.random().toString().split('.')[1];
		categories.push(categoryInDB);
	}

	return categories;
}

export function deleteCategory(id) {
	let categoryInDB = categories.find((c) => c._id === id);
	categories.splice(categories.indexOf(categoryInDB), 1);
	return categoryInDB;
}
