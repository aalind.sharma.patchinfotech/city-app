export function getUser() {
	return JSON.parse(localStorage.getItem('currentUser'));
}

export function login() {
	localStorage.setItem('currentUser', JSON.stringify(true));
}

export function logout() {
	localStorage.setItem('currentUser', JSON.stringify(''));
}
