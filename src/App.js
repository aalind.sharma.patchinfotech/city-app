import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Notfound from './components/NotFound';
import Login from './components/Login/Login';
import Otp from './components/Otp/Otp';
import Layout from './components/layout/layout';

import './App.css';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: '',
		};
	}

	componentDidMount = () => {
		let user = JSON.parse(localStorage.getItem('currentUser'));
		this.setState({ user });
	};

	render() {
		return (
			<div>
				<Switch>
					<Route path='/otp' component={Otp} />
					<Route path='/login' component={Login} />
					<Route path='/not-found' component={Notfound} />
					<Route component={Layout} />
				</Switch>
			</div>
		);
	}
}

export default App;
